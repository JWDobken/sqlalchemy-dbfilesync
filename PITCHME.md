# Flux 

An application architecture for React

---

### Flux Design

- Dispatcher: Manages Data Flow
- Stores: Handle State & Logic
- Views: Render Data via React

---
iterative design process

small changes that take us from a working state to a working state
---

![TDD cycle](https://www.obeythetestinggoat.com/book/images/twp2_0701.png)


---
### Authentication

https://medium.com/python-rest-api-toolkit/python-rest-api-authentication-with-json-web-tokens-1e06e449f33